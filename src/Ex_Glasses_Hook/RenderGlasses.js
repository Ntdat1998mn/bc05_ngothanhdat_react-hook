import React from "react";
import { useState } from "react";
import { dataGlasses } from "./dataGlasses";

export default function RenderGlasses() {
  let [glasses, setGlasses] = useState(dataGlasses[0]);
  /* Render lan dau */
  let renderListGlasses = () => {
    return dataGlasses.map((item) => {
      return (
        <div
          key={item.id}
          onClick={() => {
            setGlasses(item);
          }}
        >
          <img src={item.url} alt="" />
        </div>
      );
    });
  };
  return (
    <div className="myBody">
      <div className="model">
        <div className="img">
          <img src="./glassesImage/model.jpg" />
          <img className="glasses__img" src={glasses.url} />
          <div className="info">
            <h4>{glasses.name}</h4>
            <p>{glasses.desc}</p>
          </div>
        </div>
        <div className="img">
          <img src="./glassesImage/model.jpg" />
        </div>
      </div>
      <div className="glasses">{renderListGlasses()}</div>
    </div>
  );
}
