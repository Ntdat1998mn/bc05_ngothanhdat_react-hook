import React, { Component } from "react";
import RenderGlasses from "./RenderGlasses";

export default class Ex_Glasses_Hook extends Component {
  render() {
    return (
      <div className="glasses__layout">
        <div className="header">Try glasses app online</div>
        <RenderGlasses />
        <div className="glasses__layout-overlay"></div>
      </div>
    );
  }
}
